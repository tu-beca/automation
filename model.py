# -*- coding: utf-8 -*-
"""
Created on Mon Apr  2 00:16:16 2018

@author: oscar
"""

import numpy as np
import pandas as pd
import matplotlib as plt
import data_curation as dc
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from ggplot import *

modelIndices = [0,2,4,5]

def plotData(X, Y):
    pca = PCA(n_components = 2)
    pcomponents = pca.fit_transform(X)
    
    plt.pyplot.xlabel("X data")
    plt.pyplot.ylabel("Y values")
    
    plt.pyplot.scatter(pcomponents[:,0], Y, c = (Y >= 0.85))
    plt.pyplot.show()
    

def createRandomTrainingSet(X, Y):
    trainIndices = np.arange(int(X.shape[0] * 0.7))
    np.random.shuffle(trainIndices)

    Xtrain = X[trainIndices, :]
    Ytrain = Y[trainIndices, :]
    
    Xremaining = X[[i for i in xrange(X.shape[0]) if i not in trainIndices], :]
    Yremaining = Y[[i for i in xrange(Y.shape[0]) if i not in trainIndices], :]
    
    testIndices = np.arange(Xremaining.shape[0])
    np.random.shuffle(testIndices)
    
    Xtest = Xremaining[testIndices, :]
    Ytest = Yremaining[testIndices, :]

    crossIndices1 = np.arange(int(Xtrain.shape[0] * (1.0 / 7.0)))    
    crossIndices2 = np.arange(int(Xtest.shape[0] * (1.0 / 3.0)))    
    
    np.random.shuffle(crossIndices1)
    np.random.shuffle(crossIndices2)
    
    Xcross = np.concatenate((Xtrain[crossIndices1, :], Xtest[crossIndices2, :]))
    Ycross = np.concatenate((Ytrain[crossIndices1, :], Ytest[crossIndices2, :]))
    
    return (Xtrain, Ytrain, Xcross, Ycross, Xtest, Ytest, trainIndices, testIndices)



def main():
    X, Y, scaler = dc.mapStudentsToGrades()
    
    X = X[:, modelIndices]
    
    Xtrain, Ytrain, Xcross, Ycross, Xtest, Ytest, trainIndices, testIndices = createRandomTrainingSet(X, Y)
    
    regressor = RandomForestRegressor(max_depth = 42, random_state = 0)
    regressor.fit(Xtrain, Ytrain[:, 0])
    
    YtrainPredicted = regressor.predict(Xtrain)
    YcrossPredicted = regressor.predict(Xcross)
    YtestPredicted = regressor.predict(Xtest)
    
    print("Training accuracy: " + str(r2_score(Ytrain[:, 0], YtrainPredicted)))
    print("Cross validation accuracy: " + str(r2_score(Ycross[:, 0], YcrossPredicted)))
    print("Test accuracy: " + str(r2_score(Ytest[:, 0], YtestPredicted)))
    #print(Xcross.shape)
    
    plotData(Xtrain, YtrainPredicted)    
    
if __name__ == "__main__" : main()