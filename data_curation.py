import numpy as np
import pandas as pd
from sklearn import preprocessing
import re

responsesFile = "data/applicant_data.csv"
gradingFile = "data/grading_2017.csv"
finalistsFile = "data/finalists.csv"


def mapStudentsToGrades():
    data = np.zeros(shape = (880, 8), dtype = np.float64)
    results = np.zeros(shape = (880, 6), dtype = np.float64)
    personalInfo = np.empty(shape = (0,1))
    
    responsesDf = pd.read_csv(responsesFile, quotechar='"', skipinitialspace = True)
    responses = responsesDf.values
    grades = pd.read_csv(gradingFile, quotechar='"', skipinitialspace = True).values
    finalists = pd.read_csv(finalistsFile, quotechar='"', skipinitialspace = True)
    resultCount = 0    
    
    maxEssayLength = 100
    minEssayLength = 5    
    
    maxGrade = 85.0
    minGrade = 51.0
    
    winnerCount = 0
    
    for j in range(responses.shape[0]):
                           
        if "lico" in responses[j, 14]:  # Public or private school
            data[resultCount, 0] = 2.0
        else:
            data[resultCount, 0] = 1.5
            
        if "lica" in responses[j, 16]: #Public or private university
            data[resultCount, 1] = 2.0
        else:
            data[resultCount, 1] = 1.5
            
        data[resultCount, 0] += data[resultCount, 1]  # socio-economical status
            
        if not responses[j, 18]:  # University marks
            data[resultCount, 2] = 0
        else:
            data[resultCount, 2] = float(responses[j, 18])
            
        if "Trabajando" in responses[j, 19]:  # Employment status
            data[resultCount, 3] = 1.5
        else:
            data[resultCount, 3] = 2.0
            
        data[resultCount, 4] = len(re.findall(r'\w+', responses[j, 20])) # Essay length (To be improved)
        
        if responses[j, 22] == "Superior":  # Language level
            data[resultCount, 5] = 2
        elif responses[j, 22] == "Medio-superior":
            data[resultCount, 5] = 1
        
        if isinstance(responses[j, 23], basestring) and responses[j, 23][0] == 'S': #Availability of certificate
            data[resultCount, 5] = 2
        
        if isinstance(responses[j, 25], basestring) and responses[j, 25][0] == 'S': #Availability to purchase certificate
            data[resultCount, 7] = 1
            
        # NEW MAPPINGS
            
        results[resultCount, 1] = ((min(float(data[resultCount, 2] if data[resultCount, 2] >= minGrade else minGrade), maxGrade) - minGrade) / (maxGrade - minGrade)) * 45.0

        if (data[resultCount, 5] == 2):
            results[resultCount, 2] = 40.0
        elif (data[resultCount, 5] == 1):
            results[resultCount, 2] = 20.0
        else:
            results[resultCount, 2] = 0
            
        results[resultCount, 3] = (float(data[resultCount, 0]) - 3.0) * 5.0  

        essayLength = data[resultCount, 4]
        essayLength = essayLength if essayLength <= maxEssayLength else maxEssayLength
        essayLength = essayLength if essayLength >= minEssayLength else minEssayLength
        
        results[resultCount, 4] = ((essayLength - minEssayLength) / (maxEssayLength - minEssayLength)) * 10.0
                               
        res = results[resultCount,1] + results[resultCount,2] + results[resultCount,3] + results[resultCount,4]
        results[resultCount, 0] = res / 100
        
        if results[resultCount, 0] >= 0.85:
            results[resultCount,5] = 1
            personalInfo = np.append(personalInfo, responses[j, 0])
            winnerCount += 1
        else:
            results[resultCount,5] = 0

        resultCount += 1

    resultDf = pd.DataFrame(np.c_[results[np.where(results[:, 5] == 1)], personalInfo], 
                                          columns = ["Total grade", "Univ. marks", "Language", "Socioeco. status", "Essay", "Winner", "E-mail"])
    resultDf.to_csv("data/winners_expected.csv")

    print("Winners " + str(winnerCount))

    scalerX = preprocessing.StandardScaler().fit(data)
    data = scalerX.transform(data)
    #results = preprocessing.MinMaxScaler().fit_transform(results)
    
    idx = 73
    print (data[idx])                
    print (results[idx])
                
    return (data, results, scalerX)

def main():
    mapStudentsToGrades()

if __name__ == "__main__" : main()
    
